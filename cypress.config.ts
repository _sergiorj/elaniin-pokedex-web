import { defineConfig } from 'cypress';

if (process.env.NODE_ENV === 'development') {
  require('dotenv').config();
}

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:3000',
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
