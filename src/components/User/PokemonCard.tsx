import React from 'react';
import Image from 'next/image';
import img404 from '../../../public/images/404.png';

export default function PokemonCard({
  pokemon,
  selectPokemon,
  deletePokemon,
  list,
}: any) {
  return (
    <section
      key={pokemon.id}
      role="article"
      className="flex flex-row bg-white rounded shadow-md"
    >
      <section role="article">
        <Image
          role="img"
          src={pokemon.sprites.front_default || img404}
          width={150}
          height={150}
          alt={pokemon.name}
        />
        <h1 className="mb-5 font-bold text-center text-blue-800 font-montserrat">
          {pokemon.name.toUpperCase()}
        </h1>
      </section>
      <section
        role="article"
        className="grid content-center pr-4 ml-3 w-[107px]"
      >
        <h1 className="mb-2 text-2xl font-bold text-center">Stats</h1>
        <h2 className="mb-1 text-center text-pink-500 bg-pink-100 rounded">
          <span className="font-bold">Exp.</span> {pokemon.base_experience}
        </h2>
        <h2 className="text-center text-violet-500 bg-violet-100">
          <span className="font-bold">Wg.</span> {pokemon.weight}
        </h2>
        {list.find((item: any) => item.id === pokemon.id) ? (
          <button
            data-testid="remove-pokemon-button"
            role="button"
            className="text-red-500 bg-red-100 border border-red-400 hover:text-black hover:bg-red-300 mt-7"
            onClick={() => deletePokemon(pokemon.id)}
          >
            <span>Remove</span>
          </button>
        ) : (
          <button
            data-testid="add-pokemon-button"
            role="button"
            className="text-green-500 bg-green-100 border border-green-400 hover:text-black hover:bg-green-300 mt-7"
            onClick={() => selectPokemon(pokemon)}
          >
            <span>Add</span>
          </button>
        )}
      </section>
    </section>
  );
}
