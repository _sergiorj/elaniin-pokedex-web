import React, { useState, useContext, useEffect } from 'react';
import PokemonContext from '../Context/PokemonContext';
import { regionList } from '../../data/RegionList';
import CardRegions from './CardRegions';

export default function PokemonRegions({ setSteps }: any) {
  const {
    regionName,
    setRegionName,
    setSubtitleCreateTeam,
    setPokemonSavedList,
  } = useContext<any>(PokemonContext);

  const [changingPage, setChangingPage] = useState(false);

  useEffect(() => {
    setSubtitleCreateTeam('Select a region.');
    if (changingPage) {
      setSteps(2);
    }
  }, [changingPage]);

  const handleRegion = (region: string) => {
    if (regionName !== region) {
      setPokemonSavedList([]);
      setSubtitleCreateTeam('');
    }
    setRegionName(region);
    setChangingPage(true);

    setSubtitleCreateTeam('Pick your Pokemons, minimum 3 and maximum 6.');
  };

  return (
    <section role="article" className="h-screen font-montserrat">
      <section
        role="article"
        className="grid grid-cols-2 gap-4 mb-16 2xl:grin-cols-7 xl:grid-cols-6 lg:grid-cols-5 md:grid-cols-4 sm:grid-cols-3 "
      >
        <CardRegions
          regionList={regionList}
          regionName={regionName}
          handleRegion={handleRegion}
        />
      </section>
    </section>
  );
}
