import React from 'react';
import Image from 'next/image';

export default function CardRegions({
  regionList,
  regionName,
  handleRegion,
}: any) {
  return (
    <>
      {regionList?.map((region: any) => (
        <section
          role="button"
          onClick={() => handleRegion(region.name)}
          key={region.id}
          className={` ${
            regionName === region.name
              ? 'bg-green-500 p-2 rounded'
              : 'transition duration-300 transform hover:scale-105'
          } `}
        >
          <Image role="img" src={region.img} alt={region.name} priority />
        </section>
      ))}
    </>
  );
}
