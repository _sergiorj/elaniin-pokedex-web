import React, { useContext, useEffect, useState } from 'react';
import TitleUser from './TitleUser';
import SubtitleUser from './SubtitleUser';
import PokemonRegions from './PokemonRegions';
import PokemonByRegion from './PokemonByRegion';
import PokemonCreateTeam from './PokemonCreateTeam';
import PokemonContext from '../Context/PokemonContext';

interface Props {
  title: string;
}

export default function StepsCreateTeam({ title }: Props) {
  const [steps, setSteps] = useState(1);
  const { subtitleCreateTeam } = useContext<any>(PokemonContext);

  return (
    <section className="h-full">
      <TitleUser title={title} />
      <SubtitleUser creationPart={steps} subTitle={subtitleCreateTeam} />
      {steps === 1 ? (
        <PokemonRegions setSteps={setSteps} />
      ) : steps === 2 ? (
        <PokemonByRegion setSteps={setSteps} />
      ) : (
        <PokemonCreateTeam setSteps={setSteps} />
      )}
    </section>
  );
}
