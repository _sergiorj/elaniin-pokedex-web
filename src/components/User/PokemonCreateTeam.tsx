import React from 'react';
import usePokemonCreateTeam from '../Hooks/usePokemonCreateTeam';

export default function PokemonCreateTeam({ setSteps }: any) {
  const { inputText, setInputText, handleCreateTeam } = usePokemonCreateTeam();

  return (
    <section className="h-screen">
      <section role="article" className="flex justify-center">
        <section className="bg-white h-[350px] rounded-lg shadow-lg pb-28 px-28">
          <h1 className="text-3xl font-bold text-center mb-14 mt-14 font-montserrat">
            {inputText}
          </h1>
          <input
            type="text"
            autoComplete="off"
            placeholder="Team Name"
            value={inputText}
            onChange={(e) => setInputText(e.target.value)}
            className="h-10 px-3 mb-5 mr-3 text-base text-gray-700 placeholder-gray-600 border rounded-lg w-80 focus:shadow-outline"
          />
          <button
            disabled={inputText.length === 0}
            onClick={() => handleCreateTeam()}
            role="button"
            type="submit"
            value="/user"
            className={`h-10 px-3 mb-5 ${
              inputText.length === 0
                ? 'bg-slate-400 text-slate-100'
                : 'bg-green-500 text-green-100'
            } rounded-lg focus:shadow-outline`}
          >
            <span>Save Team</span>
          </button>
        </section>
      </section>
      <button
        type="button"
        role="button"
        onClick={() => setSteps(2)}
        className={`grid px-3 py-3 m-auto mt-24 rounded bg-blue-500 text-cyan-100`}
      >
        <span className="font-bold text-center text-white">Go Back</span>
      </button>
    </section>
  );
}
