import React from 'react';

interface Props {
  title: string;
}

export default function TitleUser({ title }: Props) {
  return (
    <>
      <h1 className="mt-10 text-2xl font-bold text-gray-900 font-montserrat mb-7">
        {title}
      </h1>
    </>
  );
}
