import React, { useState, useEffect } from 'react';
import MenuIcon from '../Icons/MenuIcon';
import Image from 'next/image';

import { fetchUserPhoto, fetchUserName } from '../../data/LocalStorage';

interface Props {
  showNav: boolean;
  setShowNav: (showNav: boolean) => void;
}

export default function TopBar({ showNav, setShowNav }: Props) {
  const [userPhoto, setUserPhoto] = useState('');
  const [username, setUsername] = useState('');

  useEffect(() => {
    const imgRoot = fetchUserPhoto();
    const userName = fetchUserName();
    setUserPhoto(imgRoot);
    setUsername(userName);
  }, []);

  return (
    <section
      role="menubar"
      className={`fixed w-full h-16 flex bg-white justify-between items-center transition-all duration-[400ms] ${
        showNav ? 'pl-56' : ''
      }`}
    >
      <section className="pl-4 md:pl-16">
        <section role="button" onClick={() => setShowNav(!showNav)}>
          <MenuIcon />
        </section>
      </section>
      <section className="flex items-center pr-4 md:pr-16">
        <section className="relative inline-block text-left">
          <section
            role="group"
            className="inline-flex items-center justify-center w-full"
          >
            {userPhoto && (
              <section className="border-2 shadow-sm md:mr-2">
                <Image
                  role="img"
                  src={userPhoto}
                  width={35}
                  height={35}
                  alt="profile picture"
                />
              </section>
            )}
            <h1 className="hidden font-medium text-black font-montserrat md:block">
              {username}
            </h1>
          </section>
        </section>
      </section>
    </section>
  );
}
