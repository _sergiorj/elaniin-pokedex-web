import React from 'react';

export default function Card({ datas, deleteTeam, editTeam }) {
  return (
    <section
      role="article"
      className="grid gap-5 mb-16 2xl:grid-cols-2 xl:grid-cols-1 lg:grid-cols-1"
    >
      {datas?.map((item) => {
        return (
          <section
            role="article"
            key={item.id}
            className="bg-white rounded shadow-sm "
          >
            <section
              role="article"
              className="flex flex-row justify-between px-4 py-4"
            >
              <section role="article">
                <h1>
                  <span className="text-2xl font-bold text-black">
                    {item.name}
                  </span>
                </h1>
                <h1>
                  Region{' '}
                  <span className="font-bold text-black">{item.region}</span>
                </h1>
              </section>
              <section role="article">
                <section
                  role="button"
                  className="p-2 mb-1 text-white bg-red-400 rounded hover:text-black hover:bg-red-500 "
                >
                  <span onClick={() => deleteTeam(item.id)}>
                    <h3 className="text-center">DELETE</h3>
                  </span>
                </section>
                <section
                  role="button"
                  className="p-2 text-white bg-green-400 rounded hover:text-black hover:bg-green-500"
                >
                  <span onClick={() => editTeam(item)}>
                    <h3 className="text-center">EDIT</h3>
                  </span>
                </section>
              </section>
            </section>
            <section
              role="article"
              className="grid grid-cols-2 pb-3 md:grid-cols-4 lg:grid-cols-6 xl:grid-cols-6 2xl:grid-cols-6"
            >
              {item.pokemon.map((pokemon) => {
                return (
                  <section key={pokemon.id} className="flex flex-row">
                    <section className="m-auto">
                      <img
                        role="button"
                        className="w-12 h-12 m-auto"
                        src={pokemon.sprites.front_default}
                        alt={pokemon.name}
                      />
                      <h3 className="mb-1 text-center text-green-500 bg-green-100 rounded">
                        <span className="font-bold"></span>{' '}
                        {pokemon.species.name}
                      </h3>
                      <h3 className="mb-1 text-center text-pink-500 bg-pink-100 rounded">
                        <span className="font-bold">Exp.</span>{' '}
                        {pokemon.base_experience}
                      </h3>
                      <h3 className="mb-1 text-center rounded text-violet-500 bg-violet-100">
                        <span className="font-bold">Wg.</span> {pokemon.weight}
                      </h3>
                      <h3 className="mb-1 text-center text-blue-500 bg-blue-100 rounded">
                        <span className="font-bold">Hg.</span> {pokemon.height}
                      </h3>

                      <h3 className="font-bold text-center">
                        {pokemon.name.toUpperCase()}
                      </h3>
                    </section>
                  </section>
                );
              })}
            </section>
          </section>
        );
      })}
    </section>
  );
}
