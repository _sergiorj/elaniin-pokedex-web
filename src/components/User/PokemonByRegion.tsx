import React from 'react';
import PokemonCard from './PokemonCard';
import usePokemonByRegion from '../Hooks/usePokemonByRegion';

export default function PokemonByRegion({ setSteps }: any) {
  const {
    loadMore,
    handlePokemonSeleted,
    handleDeletePokemon,
    pokemonList,
    pokemonSavedList,
    pokeCount,
    maxPoke,
  } = usePokemonByRegion();

  const handleSaveTeam = () => {
    if (pokemonSavedList?.length <= 2) {
      alert('You must select 3 to 6 pokemons');
    } else {
      setSteps(3);
    }
  };

  return (
    <section className="h-full pb-5 font-montserrat">
      <section className="grid grid-cols-1 gap-4 mb-16 place-items-center 2xl:grid-cols-4 xl:grid-cols-3 lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-2">
        {pokemonList?.map((pokemon: any) => (
          <PokemonCard
            key={pokemon.id}
            pokemon={pokemon}
            list={pokemonSavedList}
            selectPokemon={handlePokemonSeleted}
            deletePokemon={handleDeletePokemon}
          />
        ))}
      </section>
      <button
        type="button"
        role="button"
        onClick={() => loadMore()}
        className={`grid px-3 py-3 m-auto mb-12 rounded transition duration-300 transform hover:scale-105 ${
          pokeCount >= maxPoke
            ? 'bg-slate-400 text-slate-100'
            : 'bg-cyan-500 text-cyan-100 hover:text-black'
        } `}
      >
        <span className="font-bold text-center ">Load more Pokemsons</span>
      </button>
      <section className="flex flex-row">
        <button
          type="button"
          role="button"
          onClick={() => setSteps(1)}
          className={`grid px-3 py-3 m-auto mb-12 rounded bg-blue-500 text-white transition duration-300 transform hover:scale-105 hover:text-black`}
        >
          <span className="font-bold text-center">Go Back</span>
        </button>
        <button
          disabled={pokemonSavedList.length <= 2}
          type="button"
          role="button"
          onClick={() => handleSaveTeam()}
          className={`grid px-3 py-3 m-auto mb-12 rounded ${
            pokemonSavedList.length <= 2
              ? 'bg-slate-400 text-slate-100 hidden'
              : 'bg-green-500 text-green-100  transition duration-300 transform hover:scale-105 hover:text-black'
          } `}
        >
          <span className="font-bold text-center">Select Pokemons</span>
        </button>
      </section>
    </section>
  );
}
