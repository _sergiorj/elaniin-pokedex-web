import React from 'react';

export default function SubtitleUser({ creationPart, subTitle }: any) {
  return (
    <h3 className="mb-10">
      <label className="mr-3 font-bold">Part {creationPart}/3</label>
      <br />
      {subTitle}
    </h3>
  );
}
