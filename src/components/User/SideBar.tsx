import React, { forwardRef } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Image from 'next/image';
import logoImg from '../../../public/images/Pokedex_logo.png';
import HomeIcon from '../Icons/HomeIcon';
import CreateIcon from '../Icons/CreateIcon';
import useUserAuth from '../Hooks/useUserAuth';

interface Props {
  showNav: boolean;
}

const SideBar = forwardRef(({ showNav }: Props, ref: any) => {
  const { signOut } = useUserAuth();

  const router = useRouter();

  return (
    <section ref={ref} className={`fixed w-56 h-full bg-white shadow-sm`}>
      <section
        role="img"
        tabIndex={0}
        className="flex justify-center mt-6 mb-14"
      >
        <Link role="link" href="/user">
          <Image
            role="img"
            src={logoImg}
            alt="Pokedex Logo"
            className="w-[130px]"
            priority
          />
        </Link>
      </section>

      <section className="flex flex-col">
        <Link href="/user">
          <section
            role="button"
            className={`pl-6 py-3 mx-5 rounded text-center cursor-pointer mb-3 flex items-center transition-colors ${
              router.pathname == '/user'
                ? 'bg-orange-100 text-orange-500'
                : 'text-gray-400 hover:bg-orange-100 hover:text-orange-500'
            }`}
          >
            <section className="mr-2">
              <HomeIcon />
            </section>
            <section>
              <label className="font-normal font-montserrat">Home</label>
            </section>
          </section>
        </Link>
        <Link href="/user/create">
          <section
            role="button"
            className={`pl-6 py-3 mx-5 rounded text-center cursor-pointer mb-3 flex items-center transition-colors ${
              router.pathname == '/user/create'
                ? 'bg-orange-100 text-orange-500'
                : 'text-gray-400 hover:bg-orange-100 hover:text-orange-500'
            }`}
          >
            <section className="mr-2">
              <CreateIcon />
            </section>
            <section>
              <label className="font-normal font-montserrat">Create Team</label>
            </section>
          </section>
        </Link>

        <section
          role="button"
          onClick={signOut}
          className={`pl-6 py-3 mx-5 mt-20 rounded text-center bg-red-400 hover:bg-red-600 text-white cursor-pointer mb-3 flex items-center transition-colors`}
        >
          <span className="font-bold text-center text-black font-montserrat ">
            Logout
          </span>
        </section>
      </section>
    </section>
  );
});

SideBar.displayName = 'SideBar';

export default SideBar;
