import React from 'react';
import Card from './Card';
import EmptyTeams from './EmptyTeams';
import useHomePokemons from '../Hooks/useHomePokemons';

export default function TeamCard() {
  const { datas, deleteTeam, editTeam, isLoading } = useHomePokemons();

  if (isLoading) {
    return (
      <section className="w-full m-auto mt-36">
        <h1 className="p-5 text-xl font-bold text-center font-montserrat rounded-3xl">
          Loading...
        </h1>
      </section>
    );
  }
  return (
    <>
      {datas.length === 0 ? (
        <EmptyTeams />
      ) : (
        <Card datas={datas} deleteTeam={deleteTeam} editTeam={editTeam} />
      )}
    </>
  );
}
