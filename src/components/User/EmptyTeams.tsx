import React from 'react';
import Link from 'next/link';

export default function EmptyTeams() {
  return (
    <section role="article" className="m-auto text-center mt-28">
      <h1 className="mb-3 text-xl font-bold text-black font-montserrat">
        Seems that there's not any team yet!
      </h1>
      <p className="text-sm text-black font-montserrat">
        You can create a team by clicking on the button below.
      </p>
      <button
        data-testid="create-team-button"
        role="button"
        className="p-3 bg-blue-500 rounded-md hover:bg-blue-700 mt-7"
      >
        <Link role="link" href="/user/create">
          <span className="text-sm text-white font-montserrat">
            Create a team
          </span>
        </Link>
      </button>
    </section>
  );
}
