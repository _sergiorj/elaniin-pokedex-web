import React, { useState } from 'react';
import TeamCard from './TeamCard';

export default function UserHome() {
  return (
    <section className="h-screen">
      <TeamCard />
    </section>
  );
}
