import React, { ReactNode, useEffect } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useRouter } from 'next/router';
import { getAuth } from 'firebase/auth';
import { initFirebase } from '../../lib/Firebase/FirebaseApp';

const GuardedPage = (Children) => {
  initFirebase();

  function Auth(props) {
    const auth = getAuth();
    const [user, loading] = useAuthState(auth);

    const router = useRouter();

    useEffect(() => {
      if (!loading && !user) router.replace('/login');
    }, [loading, user]);

    if (loading || !user) return null;

    return (
      <>
        <Children {...props} />
      </>
    );
  }
  return Auth;
};
export default GuardedPage;
