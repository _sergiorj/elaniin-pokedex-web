import Link from 'next/link';
import React from 'react';

interface Props {
  routesData: {
    id: number;
    name: string;
    path: string;
  }[];
}

export default function Footer({ routesData }: Props) {
  return (
    <footer className="flex flex-col items-center 2xl:flex-row xl:flex-row lg:flex-row md:flex-row mt-7 p-5 bg-[#D6E4E5] font-montserrat">
      <section role="btn" className="p-2 2xl:mr-7 xl:mr-7 lg:mr-7 md:mr-7">
        <Link role="link" href="/">
          <h3 className="text-xl font-semibold">Pokédex</h3>
        </Link>
      </section>
      <section className="flex ">
        <ul className="flex flex-col items-center 2xl:flex-row xl:flex-row lg:flex-row md:flex-row">
          {routesData.map((route: any) => (
            <li
              key={route.id}
              className="mt-5 ml-0 2xl:mt-0 2xl:ml-7 xl:mt-0 xl:ml-7 lg:mt-0 lg:ml-7 md:mt-0 md:ml-7"
            >
              <Link role="link" href={route.path}>
                <span>{route.name}</span>
              </Link>
            </li>
          ))}
        </ul>
      </section>
    </footer>
  );
}
