import React from 'react';

export default function Loading() {
  return (
    <section className="flex items-center justify-center min-h-screen">
      <section className="w-8 h-8 border-4 border-blue-200 rounded-full animate-spin" />
      <p className="ml-2">cargando...</p>
    </section>
  );
}
