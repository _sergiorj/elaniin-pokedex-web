import Image from 'next/image';
import Link from 'next/link';
import React from 'react';
import imageLanding from '../../../public/images/trainer-landing.png';

export default function LandingContent() {
  return (
    <section
      role="article"
      className="flex flex-col-reverse 2xl:flex-row 2xl:mt-7 xl:flex-row xl:mt-7 lg:flex-row lg:mt-7 md:mt-7 bg-white h-[600px] mt-20"
    >
      <section
        role="article"
        className="flex items-center px-8 text-left lg:w-1/2 md:px-12"
      >
        <section role="article">
          <h2
            id="heading-title"
            className="text-3xl font-semibold text-gray-800 font-montserrat md:text-4xl"
          >
            Build Your Own <span className="text-red-700">TEAM</span>
          </h2>
          <p
            id="paragraph-text"
            className="mt-2 text-sm text-gray-500 font-montserrat md:text-base"
          >
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis
            commodi cum cupiditate ducimus, fugit harum id necessitatibus odio
            quam quasi, quibusdam rem tempora voluptates. Cumque debitis
            dignissimos id quam vel!
          </p>
          <section
            role="article"
            className="flex justify-center m-auto mt-6 lg:justify-start"
          >
            <button
              id="btn-create-team"
              role="button"
              className="p-2 px-4 bg-gray-900 rounded hover:bg-gray-600"
            >
              <Link role="link" href="/login">
                <span className="text-sm text-white font-montserrat">
                  Create Team
                </span>
              </Link>
            </button>
          </section>
        </section>
      </section>
      <section
        role="article"
        className="w-5/6 m-auto lg:w-1/2 md:w-1/2 md:m-auto sm:w-1/2 sm:m-auto"
      >
        <Image
          role="img"
          alt="Pokémon Trainer"
          src={imageLanding}
          width={500}
          height={500}
          priority
        />
      </section>
    </section>
  );
}
