import React from 'react';
import Link from 'next/link';
import Image from 'next/image';

import logoImg from '../../../public/images/Pokedex_logo.png';
import { useRouter } from 'next/router';

export default function LandingMenu() {
  const { asPath } = useRouter();

  return (
    <header
      role="banner"
      className="z-50 flex flex-row justify-between pt-2 mb-7"
    >
      <section
        role="img"
        tabIndex={0}
        className="grid items-center justify-center "
      >
        <Link role="link" href="/">
          <Image
            role="img"
            src={logoImg}
            alt="Pokedex Logo"
            className="w-[130px]"
            priority
          />
        </Link>
      </section>
      {asPath === '/login' ? null : (
        <section className="grid items-center justify-center">
          <section
            id="btn-login-signup"
            role="button"
            tabIndex={0}
            className="p-2 px-4 bg-gray-900 rounded hover:bg-gray-600"
          >
            <Link role="link" href="/login">
              <span className="text-sm font-normal text-white font-montserrat">
                Login | Sign Up
              </span>
            </Link>
          </section>
        </section>
      )}
    </header>
  );
}
