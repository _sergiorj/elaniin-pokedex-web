import React, { useState, useEffect } from 'react';
import PokemonContext from './PokemonContext';

interface Props {
  children: React.ReactNode;
}
interface SavedPokemons {
  id: number;
  name: string;
  image: string;
  weight: number;
  base_experience: number;
}
export default function PokemonProvider({ children }: Props) {
  const [titleCreateTeam, setTitleCreateTeam] = useState('Create new team');
  const [subtitleCreateTeam, setSubtitleCreateTeam] =
    useState('Select a region.');
  // const [steps, setSteps] = useState(1);
  const [regionName, setRegionName] = useState('');
  const [pokeCount, setPokeCount] = useState(16);
  const [pokeInit, setPokeInit] = useState(0);
  const [maxPoke, setMaxPoke] = useState(0);
  const [pokemonSavedList, setPokemonSavedList] = useState<SavedPokemons[]>([]);
  const [createTeam, setCreateTeam] = useState([]);
  const [userUid, setUserUid] = useState();
  const [inputText, setInputText] = useState('');
  const [isEditing, setIsEditing] = useState(false);
  const [idEditing, setIdEditing] = useState('');

  useEffect(() => {
    if (!isEditing) {
      setRegionName('');
      setInputText('');
      setPokemonSavedList([]);
    }
  }, []);

  return (
    <PokemonContext.Provider
      value={{
        createTeam,
        idEditing,
        inputText,
        isEditing,
        maxPoke,
        pokeCount,
        pokeInit,
        pokemonSavedList,
        regionName,
        setCreateTeam,
        setIdEditing,
        setInputText,
        setIsEditing,
        setMaxPoke,
        setPokeCount,
        setPokeInit,
        setPokemonSavedList,
        setRegionName,
        setSubtitleCreateTeam,
        setTitleCreateTeam,
        setUserUid,
        subtitleCreateTeam,
        titleCreateTeam,
        userUid,
      }}
    >
      {children}
    </PokemonContext.Provider>
  );
}
