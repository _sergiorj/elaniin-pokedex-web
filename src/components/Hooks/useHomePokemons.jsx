import React, { useContext, useEffect, useState } from 'react';
import { getDatabase, ref, get, remove } from 'firebase/database';
import PokemonContext from '../Context/PokemonContext';
import { useRouter } from 'next/router';

export default function useHomePokemons() {
  const [datas, setDatas] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const {
    setIsEditing,
    setIdEditing,
    setInputText,
    setRegionName,
    setPokemonSavedList,
  } = useContext(PokemonContext);

  const route = useRouter();

  const userId =
    localStorage.getItem('uid') !== ''
      ? JSON.parse(localStorage.getItem('uid'))
      : localStorage.clear();

  const readData = async () => {
    setIsLoading(true);
    const db = getDatabase();
    const dbTeamsRef = ref(db, 'teams/');
    get(dbTeamsRef, `teams/${userId}`)
      .then((snapshot) => {
        if (snapshot.exists()) {
          const mappedTeams = Object.entries(snapshot.val()[userId || '']).map(
            ([key, value]) => ({
              id: key,
              ...value,
            })
          );
          setIsLoading(false);
          setDatas(mappedTeams);
        }
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  const deleteTeam = async (id) => {
    const db = getDatabase();
    const starCountRef = ref(db, `teams/${userId}/${id}`);
    remove(starCountRef);
    setDatas(datas.filter((item) => item.id !== id));
  };

  const editTeam = (id) => {
    setIsEditing(true);
    setIdEditing(id.id);
    setInputText(id.name);
    setRegionName(id.region);
    setPokemonSavedList(id.pokemon);
    route.replace('/user/edit');
  };

  useEffect(() => {
    readData();
  }, []);

  return {
    datas,
    deleteTeam,
    isLoading,
    editTeam,
  };
}
