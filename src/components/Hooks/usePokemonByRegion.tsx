import React, { useContext, useState } from 'react';
import PokemonContext from '../Context/PokemonContext';
import usePokemon from './usePokemon';

export default function usePokemonByRegion() {
  const [dataPokemon, setDataPokemon] = useState(1);
  const { pokemonList } = usePokemon(dataPokemon);
  const {
    setSteps,
    pokeCount,
    setPokeCount,
    pokeInit,
    setPokeInit,
    maxPoke,
    pokemonSavedList,
    setPokemonSavedList,
    setSubtitleCreateTeam,
  } = useContext<any>(PokemonContext);

  setSubtitleCreateTeam('Pick your Pokemons, minimum 3 and maximum 6.');

  const loadMore = () => {
    if (pokeInit <= maxPoke) {
      setDataPokemon(dataPokemon + 1);
      setPokeInit(pokeInit + 16);
      setPokeCount(pokeCount + 16);
    }
  };

  const handlePokemonSeleted = (pokemon: any) => {
    if (pokemonSavedList.length < 6) {
      setPokemonSavedList([...pokemonSavedList, pokemon]);
    } else {
      alert('You can only select 6 pokemons');
    }
  };

  const handleDeletePokemon = (id: number) => {
    const newPokemonList = pokemonSavedList.filter(
      (pokemon: any) => pokemon.id !== id
    );
    setPokemonSavedList(newPokemonList);
  };

  return {
    loadMore,
    handlePokemonSeleted,
    handleDeletePokemon,
    pokemonList,
    pokemonSavedList,
    pokeCount,
    maxPoke,
    setSteps,
  };
}
