import React, { useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { getDatabase, ref, set, push } from 'firebase/database';
import PokemonContext from '../Context/PokemonContext';
import { fetchUid } from '../../data/LocalStorage';

export default function usePokemonCreateTeam() {
  const {
    setPokemonSavedList,
    setRegionName,
    setSteps,
    setSubtitleCreateTeam,
    inputText,
    setInputText,
    pokemonSavedList,
    regionName,
    setIsEditing,
    isEditing,
    idEditing,
  } = useContext<any>(PokemonContext);
  const [userId, setUserId] = useState<string | null>(null);
  const route = useRouter();

  useEffect(() => {
    setSubtitleCreateTeam('Choose your team name.');
    const uid = fetchUid();
    setUserId(uid);
  }, []);

  const handleCreateTeam = () => {
    const db = getDatabase();

    if (isEditing) {
      set(ref(db, `teams/${userId}/${idEditing}`), {
        name: inputText,
        region: regionName,
        pokemon: pokemonSavedList,
      });
      setIsEditing(false);
      setRegionName('');
      setInputText('');
      setPokemonSavedList([]);
    } else {
      push(ref(db, `teams/${userId}`), {
        name: inputText,
        region: regionName,
        pokemon: pokemonSavedList,
      });
      setRegionName('');
      setInputText('');
      setPokemonSavedList([]);
    }
    route.replace('/user');
  };

  return {
    setSteps,
    inputText,
    setInputText,
    handleCreateTeam,
  };
}
