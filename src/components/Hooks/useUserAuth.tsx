import React, { useState } from 'react';
import { getApps } from 'firebase/app';
import { initFirebase } from '../../lib/Firebase/FirebaseApp';
import {
  getAuth,
  signInWithPopup,
  GoogleAuthProvider,
  FacebookAuthProvider,
} from 'firebase/auth';
import { useRouter } from 'next/router';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useEffect } from 'react';

export default function useUserAuth(logMethod?: string) {
  const [provider, setProvider] = useState<
    GoogleAuthProvider | FacebookAuthProvider
  >(new GoogleAuthProvider());

  const [username, setUsername] = useState('');

  if (getApps().length < 1) {
    initFirebase();
  }

  useEffect(() => {
    if (logMethod === 'google') {
      setProvider(new GoogleAuthProvider());
    } else if (logMethod === 'facebook') {
      setProvider(new FacebookAuthProvider());
    }
  }, [logMethod]);

  const auth = getAuth();
  const [user, loading] = useAuthState(auth);
  const router = useRouter();

  const signInWithGoogle = async () => {
    const { user } = await signInWithPopup(auth, provider);
    const { displayName, photoURL, uid } = user;
    localStorage.setItem('username', JSON.stringify(displayName));
    localStorage.setItem('photoUser', JSON.stringify(photoURL));
    localStorage.setItem('uid', JSON.stringify(uid));
  };

  const signInWithFacebook = async () => {
    const { user } = await signInWithPopup(auth, provider);
    const { displayName, photoURL, uid } = user;
    localStorage.setItem('username', JSON.stringify(displayName));
    localStorage.setItem('photoUser', JSON.stringify(photoURL));
    localStorage.setItem('uid', JSON.stringify(uid));
  };

  const signOut = async () => {
    auth
      .signOut()
      .then(() => {
        localStorage.clear();
        router.push('/login');
      })
      .then((err) => {
        console.log(err);
      });
  };

  return {
    user,
    loading,
    router,
    signInWithGoogle,
    signInWithFacebook,
    username,
    signOut,
  };
}
