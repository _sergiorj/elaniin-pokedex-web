import React, { useContext, useEffect, useState } from 'react';
import PokemonContext from '../Context/PokemonContext';

export default function usePokemon(loadMore) {
  const { regionName, pokeInit, pokeCount, setMaxPoke } =
    useContext(PokemonContext);
  const [pokemonList, setPokemonList] = useState([]);

  const loadPokemons = async () => {
    try {
      const regionLink = await fetch(
        `https://pokeapi.co/api/v2/region/${regionName}`
      ).then((resp) => {
        return resp.json();
      });
      const entriesPokemon = await fetch(regionLink.pokedexes[0].url).then(
        (resp) => {
          return resp.json();
        }
      );
      setMaxPoke(entriesPokemon.pokemon_entries.length);

      const entriesFragment = entriesPokemon.pokemon_entries.slice(
        pokeInit,
        pokeCount
      );
      const pokemonName = entriesFragment.map((item) => {
        return item.pokemon_species.name;
      });
      const pokemonData = await Promise.all(
        pokemonName.map((item) =>
          fetch(`https://pokeapi.co/api/v2/pokemon/${item}`).then((resp) =>
            resp.json()
          )
        )
      );
      if (loadMore > 1) {
        setPokemonList((pokemonList) => [...pokemonList, ...pokemonData]);
      } else {
        setPokemonList(pokemonData);
      }
    } catch (error) {}
  };

  useEffect(() => {
    loadPokemons();
  }, [loadMore, regionName]);

  return {
    pokemonList,
  };
}
