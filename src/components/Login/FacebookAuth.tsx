import React from 'react';
import useUserAuth from '../Hooks/useUserAuth';
import FacebookIcon from '../Icons/FacebookIcon';

export default function FacebookAuth() {
  const { user, router, signInWithFacebook } = useUserAuth('facebook');

  if (user) {
    router.push('/user');
  }

  return (
    <button
      id="facebook-auth-button"
      role="button"
      onClick={signInWithFacebook}
      className="w-full px-4 py-2 mt-10 tracking-wide text-black transition-colors duration-200 transform bg-white rounded-md hover:bg-[#FAF8F1] focus:outline-none focus:bg-blue-400 focus:ring focus:ring-blue-300 focus:ring-opacity-50"
    >
      <section className="absolute">
        <FacebookIcon width="26" height="26" />
      </section>
      Login with <span>Facebook</span>
    </button>
  );
}
