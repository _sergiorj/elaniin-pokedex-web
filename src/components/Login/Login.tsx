import React from 'react';
import FacebookAuth from './FacebookAuth';
import GoogleAuth from './GoogleAuth';

export default function Login() {
  return (
    <section className="bg-white dark:bg-gray-900">
      <section className="flex justify-center h-screen">
        <section className="hidden bg-cover lg:block lg:w-2/3 bg-[url(https://wallpapershdnow.com/images/games/simulation/pokemon-go/pokemon-go-wallpaper-4.jpg)]">
          <section className="flex items-center h-full px-20 bg-gray-900 bg-opacity-50 font-montserrat">
            <article>
              <h2 className="text-4xl font-bold text-white">
                Your journey start here!
              </h2>

              <p className="max-w-xl mt-3 text-gray-300">
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. In
                autem ipsa, nulla laboriosam dolores, repellendus perferendis
                libero suscipit nam temporibus molestiae
              </p>
            </article>
          </section>
        </section>

        <div
          id="login-container-auth"
          role="article"
          className="flex items-center w-full max-w-md px-6 mx-auto lg:w-2/6"
        >
          <div role="article" className="flex-1">
            <div className="text-center font-montserrat">
              <h2
                id="login-title"
                role="heading"
                className="text-4xl font-bold text-center text-gray-700 font-montserrat dark:text-white"
              >
                Pokédex
              </h2>

              <p
                id="login-subtitle"
                className="mt-3 text-gray-500 dark:text-gray-300"
              >
                Sign in to access into your account
              </p>
            </div>

            <GoogleAuth />
            <FacebookAuth />
          </div>
        </div>
      </section>
    </section>
  );
}
