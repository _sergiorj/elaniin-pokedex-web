import React from 'react';
import GoogleIcon from '../Icons/GoogleIcon';
import useUserAuth from '../Hooks/useUserAuth';
export default function GoogleAuth() {
  const { user, router, signInWithGoogle } = useUserAuth('google');
  if (user) {
    router.push('/user');
  }

  return (
    <button
      id="google-auth-button"
      role="button"
      onClick={signInWithGoogle}
      className="w-full px-4 py-2 mt-14 tracking-wide text-black transition-colors duration-200 transform bg-white rounded-md hover:bg-[#FAF8F1] focus:outline-none focus:bg-blue-400 focus:ring focus:ring-blue-300 focus:ring-opacity-50"
    >
      <section className="absolute">
        <GoogleIcon width="25" height="25" />
      </section>
      Sign in with <span>Google</span>
    </button>
  );
}
