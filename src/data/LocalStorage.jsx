export const fetchUserPhoto = () => {
  const profilePhoto =
    localStorage.getItem('photoUser') !== ''
      ? JSON.parse(localStorage.getItem('photoUser'))
      : localStorage.clear();

  return profilePhoto;
};

export const fetchUserName = () => {
  const userName =
    localStorage.getItem('username') !== ''
      ? JSON.parse(localStorage.getItem('username'))
      : localStorage.clear();

  return userName;
};

export const fetchUid = () => {
  const userUid =
    localStorage.getItem('uid') !== ''
      ? JSON.parse(localStorage.getItem('uid'))
      : localStorage.clear();

  return userUid;
};
