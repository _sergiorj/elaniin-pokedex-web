export const landingRoutes = [
  {
    id: 1,
    name: 'Home',
    path: '/',
  },
  {
    id: 2,
    name: 'Login | Sing Up',
    path: '/login',
  },
];

export const userRoutes = [
  {
    id: 1,
    name: 'Home',
    path: '/user',
  },
  {
    id: 2,
    name: 'Create Team',
    path: '/user/create',
  },
];
