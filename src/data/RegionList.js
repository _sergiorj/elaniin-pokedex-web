import imgkanto from '../../public/images/regions/1.png';
import imgJohto from '../../public/images/regions/2.png';
import imgHoenn from '../../public/images/regions/3.png';
import imgSinnoh from '../../public/images/regions/4.png';
import imgUnova from '../../public/images/regions/5.png';
import imgKalos from '../../public/images/regions/6.png';
import imgAlola from '../../public/images/regions/7.png';
import imgGalar from '../../public/images/regions/8.png';
import imgHisui from '../../public/images/regions/9.png';

export const regionList = [
  {
    id: 1,
    name: 'kanto',
    img: imgkanto,
  },
  {
    id: 2,
    name: 'johto',
    img: imgJohto,
  },
  {
    id: 3,
    name: 'hoenn',
    img: imgHoenn,
  },
  {
    id: 4,
    name: 'sinnoh',
    img: imgSinnoh,
  },
  {
    id: 5,
    name: 'unova',
    img: imgUnova,
  },
  {
    id: 6,
    name: 'kalos',
    img: imgKalos,
  },
  {
    id: 7,
    name: 'alola',
    img: imgAlola,
  },
  {
    id: 8,
    name: 'galar',
    img: imgGalar,
  },
  {
    id: 9,
    name: 'hisui',
    img: imgHisui,
  },
];
