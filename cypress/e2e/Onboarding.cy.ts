describe('Test in <LandingContent />', () => {
  beforeEach(() => {
    cy.visit('/');
  });
  it('Should have text in h2.', () => {
    cy.contains('#heading-title', 'Build Your Own TEAM');
  });

  it('Should have text in p.', () => {
    cy.contains('#paragraph-text', 'Lorem ipsum dolor sit amet');
  });

  it('Should btn (Create Team) redirect to login.', () => {
    cy.get('#btn-create-team').click();
    cy.url().should('include', 'login');
    cy.visit('/');
  });
});

export {};
