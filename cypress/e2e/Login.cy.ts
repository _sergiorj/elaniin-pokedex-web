describe('Test in <Login />', () => {
  beforeEach(() => {
    cy.visit('/login');
  });

  it('Should render conponents in Login Screen', () => {
    cy.get('#login-title').should('be.visible');
    cy.get('#login-subtitle').should('be.visible');

    cy.get('#login-container-auth').then((container) => {
      const authButtons = container.find('button');
      expect(authButtons.length).to.eq(2);
    });
  });
});

export {};
