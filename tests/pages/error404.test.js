import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import Custom404 from '../../pages/404';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      route: '/',
      pathname: '',
      query: '',
      asPath: '',
    };
  },
}));

describe('Test in 404 error Page.', () => {
  it('Should match with snapshot', () => {
    const { container } = render(<Custom404 />);
    expect(container).toMatchSnapshot();
  });
});
