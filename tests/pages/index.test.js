import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import Index from '../../pages/index';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      route: '/',
      pathname: '',
      query: '',
      asPath: '',
    };
  },
}));

describe('Test in index Page.', () => {
  it('Should match with snapshot', () => {
    const { container } = render(<Index />);
    expect(container).toMatchSnapshot();
  });
});
