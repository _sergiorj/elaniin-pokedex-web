import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import TitleUser from '../../../../src/components/User/TitleUser';

describe('Test in <TitleUser /> component', () => {
  test('Should return text in h1 title.', () => {
    const title = 'TitleUser';
    render(<TitleUser title={title} />);

    const header = screen.getByRole('heading');
    expect(header).toHaveTextContent(title);
  });
});
