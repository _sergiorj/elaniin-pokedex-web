import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import CardRegions from '../../../../src/Components/User/CardRegions';
import { regionList } from '../../../../src/data/regionList';

describe('Test in <CardRegions />', () => {
  it('Should render all regions.', () => {
    const { getByAltText } = render(<CardRegions regionList={regionList} />);

    regionList.forEach((region) => {
      const image = getByAltText(region.name);
      expect(image).toBeInTheDocument();
    });
  });
});
