import React from 'react';
import { fireEvent, getByTestId, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import EmptyTeams from '../../../../src/components/User/EmptyTeams';

describe('Test in <EmptyTeams />', () => {
  test('Should return a message in (h1 & p tags).', () => {
    const { getByText } = render(<EmptyTeams />);

    const headerText = getByText(`Seems that there's not any team yet!`);
    const descriptionText = getByText(
      'You can create a team by clicking on the button below.'
    );
    const buttonText = getByText('Create a team');

    expect(headerText).toBeInTheDocument();
    expect(descriptionText).toBeInTheDocument();
    expect(buttonText).toBeInTheDocument();
  });

  test('Should click a button.', () => {
    const { container } = render(<EmptyTeams />);

    const button = getByTestId(container, 'create-team-button');
    fireEvent.click(button);
  });
});
