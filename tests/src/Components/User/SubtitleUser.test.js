import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import SubTitleUser from '../../../../src/components/User/SubtitleUser';

describe('Test in <SubtitleUser />', () => {
  test('Should return a subtitle.', () => {
    const creationPart = '1';
    const subTitle = 'SubtitleUser';

    const { getByText } = render(
      <SubTitleUser creationPart={creationPart} subTitle={subTitle} />
    );
    const subtitleUser = getByText(subTitle);
    const creationPartUser = getByText('Part 1/3');

    expect(subtitleUser).toBeVisible();
    expect(creationPartUser).toBeVisible();
  });
});
