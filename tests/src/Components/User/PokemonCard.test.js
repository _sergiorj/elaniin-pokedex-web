import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import PokemonCard from '../../../../src/components/User/PokemonCard';

describe('Test in <PokemonCard /> component.', () => {
  test('Should return a Pókemon (Image, Name, Base Experience, & Weight).', () => {
    const datas = {
      id: 1,
      name: 'Bulbasaur',
      species: 'bulbasaur',
      sprites: {
        front_default:
          'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png',
      },
      base_experience: 64,
      height: 7,
      weight: 69,
    };

    const list = [
      {
        id: 1,
      },
    ];

    const { getByText, getByAltText } = render(
      <PokemonCard pokemon={datas} list={list} />
    );

    const image = getByAltText(datas.name);
    const name = getByText(datas.name.toUpperCase());
    const baseExperience = getByText(datas.base_experience);
    const weight = getByText(datas.weight);

    expect(image).toBeInTheDocument();
    expect(name).toBeInTheDocument();
    expect(baseExperience).toBeInTheDocument();
    expect(weight).toBeInTheDocument();
  });
});
