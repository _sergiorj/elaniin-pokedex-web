import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import Card from '../../../../src/components/User/Card';

describe('Test in <Card /> component.', () => {
  test('Should return a Pókemon Team (Team Name, Region and Pokémons).', () => {
    const datas = [
      {
        id: 1,
        name: 'Team 1',
        region: 'Kanto',
        pokemon: [
          {
            id: 1,
            name: 'Bulbasaur',
            species: 'bulbasaur',
            sprites: {
              front_default:
                'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png',
            },
            base_experience: 64,
            height: 7,
            weight: 69,
          },
        ],
      },
    ];

    function deleteTeam() {
      console.log('deleteTeam');
    }

    function editTeam() {
      console.log('editTeam');
    }

    const { getByText, getByAltText } = render(
      <Card datas={datas} deleteTeam={deleteTeam} editTeam={editTeam} />
    );

    const teamName = getByText(datas[0].name);
    const region = getByText(datas[0].region);
    const image = getByAltText(datas[0].pokemon[0].name);

    expect(teamName).toBeVisible();
    expect(region).toBeVisible();
    expect(image).toBeVisible();
  });
});
