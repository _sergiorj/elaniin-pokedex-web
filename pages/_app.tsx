import '../styles/globals.css';
import type { AppProps } from 'next/app';
import PokemonProvider from '../src/components/Context/PokemonProvider';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <PokemonProvider>
      <Component {...pageProps} />
    </PokemonProvider>
  );
}
