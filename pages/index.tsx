import React from 'react';
import MainLayout from '../src/components/Layouts/MainLayout';
import LandingMenu from '../src/components/Headers/LandingMenu';
import LandingContent from '../src/components/LandingPage/LandingContent';

export default function Home() {
  return (
    <MainLayout titleText="Pokédex Home">
      <LandingMenu />
      <LandingContent />
    </MainLayout>
  );
}
