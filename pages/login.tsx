import React from 'react';
import MainLayout from '../src/components/Layouts/MainLayout';
import LandingMenu from '../src/components/Headers/LandingMenu';
import Login from '../src/components/Login/Login';

export default function login() {
  return (
    <MainLayout titleText="Pokédex Login">
      <LandingMenu />
      <Login />
    </MainLayout>
  );
}
