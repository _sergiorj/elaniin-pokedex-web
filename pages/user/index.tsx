import React from 'react';
import GuardedPage from '../../src/components/Guard/GuardedPage';
import UserLayout from '../../src/components/Layouts/UserLayout';
import TitleUser from '../../src/components/User/TitleUser';
import UserHome from '../../src/components/User/UserHome';

export function index() {
  return (
    <UserLayout>
      <TitleUser title="Your Teams" />
      <UserHome />
    </UserLayout>
  );
}

export default GuardedPage(index);
