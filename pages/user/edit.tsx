import React from 'react';
import GuardedPage from '../../src/components/Guard/GuardedPage';
import UserLayout from '../../src/components/Layouts/UserLayout';
import StepsCreateTeam from '../../src/components/User/StepsCreateTeam';

export function edit() {
  return (
    <UserLayout>
      <StepsCreateTeam title="Update your team" />
    </UserLayout>
  );
}

export default GuardedPage(edit);
