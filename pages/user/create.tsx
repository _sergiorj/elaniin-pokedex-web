import React from 'react';
import GuardedPage from '../../src/components/Guard/GuardedPage';
import UserLayout from '../../src/components/Layouts/UserLayout';
import StepsCreateTeam from '../../src/components/User/StepsCreateTeam';

export function create() {
  return (
    <UserLayout>
      <StepsCreateTeam title="Create new team" />
    </UserLayout>
  );
}

export default GuardedPage(create);
