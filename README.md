# Pokédex

![Image](landing.png)

Pokédex is a web app that allows you to create, edit and delete all the teams you want by region, with an amazing UI 👀.

## Getting Started

- First, add node packages 📦.

```bash
yarn
# or
npm install
```

- Second, create .env file in root `/.env` or `/.env.local` and add variables with your firebase credentials 🔐.

```bash
NEXT_PUBLIC_API_KEY=
NEXT_PUBLIC_AUTH_DOMAIN=
NEXT_PUBLIC_PROJECY_ID=
NEXT_PUBLIC_STORAGE_BUCKET=
NEXT_PUBLIC_MESSAGING_SENDER_ID=
NEXT_PUBLIC_APP_ID=
NEXT_PUBLIC_DATABASE_URL=
```

- Third, run the development server 💻.

```bash
yarn dev
# or
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Testing 🧪

- First, it should be running the project in local host [http://localhost:3000], follow the steps above to do it.

- Second, run the tests you want to try.

```bash
## Jest Tests

yarn test
#or
npm test

## Cypress Tests

yarn cypress:run
#or
npm cypress:run

```

## Docker 🐳

- To create a Docker image, make sure that Docker Desktop is running before entering the following line.

```bash
docker build -t [project-name] .
```

- Run container

```bash
docker run -p 3000:3000 [project-name]
```
